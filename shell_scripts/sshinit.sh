#!/bin/bash

sshinit () {
	
	eval $(ssh-agent -s)

	ssh-add ~/.ssh/ssh_github
	echo Checking\ connection\ to\ github.com ...
	ssh -T git@github.com

	ssh-add ~/.ssh/ssh_gitlab
	echo Checking\ connection\ to\ gitlab.com ...
	ssh -T git@gitlab.com

	ssh-add ~/.ssh/ssh_uni	
	echo Checking\ connection\ to\ ecegit.ee.bgu.ac.il ...
	ssh -T git@ecegit.ee.bgu.ac.il
}
